from hashlib import md5


def output_msg_with_its_md5(msg):
    return "{} {}".format(msg, md5(str(msg).encode('utf-8')).hexdigest()) if msg else ""

