from webapp import app
from webapp import utils
from urllib.parse import unquote_plus


@app.route('/message', defaults={'url_msg': ''}, methods=['GET'])
@app.route('/message/<url_msg>')
def index(url_msg):
    return utils.output_msg_with_its_md5(unquote_plus(url_msg, encoding='utf-8'))
