from webapp import app as create_app
from frontend.tests import data
import pytest


@pytest.fixture
def app():
    app = create_app
    app.config['DEBUG'] = True
    app.app_context().push()
    yield app


@pytest.fixture
def client(app):
    with app.test_client() as client:
        yield client


def test_get_utf8_response(client):
    response = client.get('/message/%s' % data.UTF8_STR)
    assert response.status_code == 200
    assert response.data.decode('utf-8') == "%s %s" % (data.UTF8_STR, data.MD5_UTF8_STR)


if __name__ == "__main__":
    test_get_utf8_response(client)
