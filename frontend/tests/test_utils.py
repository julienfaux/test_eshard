from webapp import utils
from frontend.tests import data


def test_utf8_str():
    assert '%s %s' % (data.UTF8_STR, data.MD5_UTF8_STR) == utils.output_msg_with_its_md5(data.UTF8_STR)


def test_empty_str():
    assert data.EMPTY_STR == utils.output_msg_with_its_md5(data.EMPTY_STR)
