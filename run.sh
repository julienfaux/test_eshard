#!/bin/bash

#start minikube
minikube start

# configure the local environment to re-use the Docker daemon inside the Minikube instance
eval $(minikube docker-env)

# create cluster resource from manifest
kubectl create -f deployment/manifest.yml

# find
_port=`kubectl get svc | awk '/5000:/ {print $5}' | grep -oP '5000:\K.*?(?=/TCP)'`
_host=`kubectl cluster-info | awk '/at / {print $7}' | grep -oP 'https:\K.*?(?=:)'`

# export cluster url to pass it to scenario.py
export kube_target=http:$_host:$_port

# call scenario.py
python3 scripts/scenario.py
