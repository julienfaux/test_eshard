# A. prerequisites

sudo ability or docker & kubernetes already installed

## 1. get docker in local
```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker $(whoami)
```

## 2. get kubectl in local
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

# B. fully automated POC

launch run.sh to install, start kubernetes pods & send 8 simultaneous requests

```
./run.sh
```


# B. manual step by step build

## 1. cloning the repo
```
git clone https://gitlab.com/julienfaux/test_eshard.git
cd test_eshard
```

## 2. frontend app

### install dependencies
```
sudo apt-get update && apt-get install python3-pip
python3 -m pip install -r frontend/requirements.txt
```

### run tests
```
pytest
```
### launch flask app (from frontend folder)
```
python3 wsgi.py
```

## 3. docker image part

### pull from docker hub
```
docker pull julienfaux/eshard_flask_image:latest
```

### ...or build & run from Dockerfile
```
docker build -t  julienfaux/eshard_flask_image:latest .
docker run -d -p 5000:5000 julienfaux/eshard_flask_image
```


## 4. kubernetes part

### start kubernetes nodes
```
minikube start
eval $(minikube docker-env)
kubectl create -f deployment/manifest.yml

```
### get host URL & port & export result
```
_port=`kubectl get svc | awk '/5000:/ {print $5}' | grep -oP '5000:\K.*?(?=/TCP)'`
_host=`kubectl cluster-info | awk '/at / {print $7}' | grep -oP 'https:\K.*?(?=:)'`
export kube_target=http:$_host:$_port
```

## 5. play scenario from exercice

```
python3 scripts/scenario.py
```
