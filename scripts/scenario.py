import os
import random
import concurrent.futures
import requests
import threading
import time
import subprocess
import logging
import sys


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s', stream=sys.stdout)

target = os.environ.get('kube_target')  # or manually add it using echo $kube_target
print("host is at %s" % target)


def complete_url_with_random_msg():
    return "%s%s%s" % (
                       target,
                       '/message/',
                       "".join(s for s in random.sample("azertyuiopqsdfghjklmwxcvbn", random.randrange(1, 10)))
                       )


urls = [complete_url_with_random_msg() for i in range(8)]


thread_local = threading.local()


def get_session():
    if not hasattr(thread_local, "session"):
        thread_local.session = requests.Session()
    return thread_local.session


def send_request(url):
    session = get_session()
    with session.get(url) as response:
        logging.info(f"Got '{response.text}' from {url}")


def send_all_requests(url):
    with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
        executor.map(send_request, url)


def pod_is_running():
    TIMEOUT = 100
    t = 0
    while t < TIMEOUT:
        pod_state=subprocess.run(['kubectl', 'get', 'pods'], capture_output=True)
        if "running" in pod_state.stdout.decode('utf-8').lower():
            return True
        else:
            time.sleep(1)
            t+=1
            logging.info("waiting running pod...")


if __name__ == "__main__":
    logging.info("NEW RUN")
    logging.info("wait for pod availability")
    if pod_is_running():
        logging.info("pod available !")
        start_time = time.time()
        send_all_requests(urls)
        duration = time.time() - start_time
        logging.info(f"sent {len(urls)} requests in {duration} seconds")
    else:
        logging.info("timeout reached")
